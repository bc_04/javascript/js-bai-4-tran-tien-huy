let main = () => {
  
  let bai_1 = () => {
    /**
   * Input:
   * - num1 = 3
   * - num2 = 40
   * - num3 = 4
   * Steps: 
   *  - Declare and Initialize 3 variables to store 3 inputs.
   *  - Declare and Initialize a variable to store output.
   *  - Using conditional statements if-elseif-else to sort 3 numbers in ascending order
   * Output: 3 < 4 < 40
   * 
   */
    let a = parseFloat(document.querySelectorAll(".bai-tap-1 .num-1")[0].value) || 0;
    let b = parseFloat(document.querySelectorAll(".bai-tap-1 .num-2")[0].value) || 0;
    let c = parseFloat(document.querySelectorAll(".bai-tap-1 .num-3")[0].value) || 0;
    let output = ``;

    if(!a && !b && !c) {
      output = `${a} < ${b} < ${c}`;
    } else {
      if(a >= b && b >= c) {
        output = `${c} < ${b} < ${a}`;
      } else if(a >= c && c >= b) {
        output = `${b} < ${c} < ${a}`;
      } else if(b >= a && a>=c) {
        output = `${c} < ${a} < ${b}`;
      } else if(b>=c && c>=a) {
        output = `${a} < ${c} < ${b}`;
      } else if(c>=a && a>=b) {
        output = `${b} < ${a} < ${c}`;
      } else {
        output = `${a} < ${b} < ${c}`;
      }
    }
    document.querySelectorAll(".txt-result-bai-1")[0].innerHTML = output;
  };

  let bai_2 = () => {
    /**
   * Input:
   * - personToGreet = 1;
   * 
   * Steps: 
   *  - Declare and Initialize a variables to store person to be greeted input.
   *  - Declare and Initialize a variable to store output.
   *  - Use conditional Statement to find out if the selected person is Bố, Mẹ, Anh Trai, Em gái.
   *  - Output = "Xin chào " + person;
   * 
   * Output: Xin chào Bố
   * 
   */
    let person_to_greet = document.querySelectorAll(".person")[0].value;
    let output = "Xin chào ";
    switch(person_to_greet) {
      case "1":
        output+= "Bố!";
        break;
      case "2":
        output+= "Mẹ!";
        break;
      case "3":
        output+= "Anh Trai!"
        break;
      case "4":
        output+= "Em Gái!"
        break;
      default: 
        output += "Người lạ ơi!";
        break;
    }
    document.querySelectorAll(
      ".txt-result-bai-2"
    )[0].innerHTML = output;
  };

  let bai_3 = () => {
    /**
     * NUMBER_EL = 3
   * Input:
   * - num_1 = 2;
   * - num_2 = 3;
   * - num_3 = 4;
   * 
   * Steps: 
   *  - Declare and Initialize 3 variables to store 3 inputs.
   *  - Declare and Initialize a variable to count even number(s).
   *  - Declare and Initialize a variable to store output.
   *  - If a number is even => Increase the count by 1;
   *  - Number of odd element = NUMBER_EL - count;
   * 
   * Output: Có 2 số chẵn, 1 số lẻ
   * 
   */
    const NUMBER_EL = 3;
    let a = parseInt(document.querySelectorAll(".bai-tap-3 .num-1")[0].value) || 0;
    let b = parseInt(document.querySelectorAll(".bai-tap-3 .num-2")[0].value) || 0;
    let c = parseInt(document.querySelectorAll(".bai-tap-3 .num-3")[0].value) || 0;
    let even_count = 0,
        output = ``;

    if(!a && !b && !c) {
      even_count = 3; 
    } else {
      (!(a%2)) && even_count++;
      (!(b%2)) && even_count++;
      (!(c%2)) && even_count++;
    }
    output = `Có ${even_count} số chẵn, ${NUMBER_EL - even_count} số lẻ`;

    document.querySelectorAll(
      ".txt-result-bai-3"
    )[0].innerHTML = output;
  };

  let bai_4 = () => {
    /**
     * 
    * Input:
    * - side_1 = 2;
    * - side_2 = 2;
    * - side_3 = 5
    * Steps: 
    *  - Declare and Initialize 3 variables to store 3 sides of triangle.
    *  - Declare and Initialize a variable to store perimeter value.
    *  - Declare and Initialize a variable to store output.
    * 
    * Output:
    *  - Check condition: 
    *     + If: a==b==c => "Tam giác đều"
    *     + Else if: a==b || b== c || a == b => "Tam giác cân"
    *     + Else if: a^2 + b^2 = c^2 => "Tam giác vuông"
    *     + Else: "Loại tam giác khác"
    *  
    * Output: Hình tam giác cân
    * 
    */
     let a = parseFloat(document.querySelectorAll(".bai-tap-4 .side-1")[0].value) || 0;
     let b = parseFloat(document.querySelectorAll(".bai-tap-4 .side-2")[0].value) || 0;
     let c = parseFloat(document.querySelectorAll(".bai-tap-4 .side-3")[0].value) || 0;
     let is_isosceles_triangle = (Math.pow(a,2) === Math.pow(b,2) + Math.pow(c,2)) 
                                || (Math.pow(b,2) === Math.pow(a,2) + Math.pow(c,2)) 
                                || (Math.pow(c,2) === Math.pow(a,2) + Math.pow(b,2)); 
     let output = "";
     if(a === b && b === c) {
      output = "Hình tam giác đều"
     } else if(a === b || b === c || a === c) {
      output = "Hình tam giác cân";
     } else if(is_isosceles_triangle) {
      output = "Hình tam giác vuông"
     } else {
      output = 'Một loại tam giác nào đó'
     }
     document.querySelectorAll(".txt-result-bai-4")[0].innerHTML = output;
  };

  let bai_5_get_previous_day = () => {
    /**
     * 
   * Input:
   * - day = 1
   * - month = 1
   * - year = 2023
   * Steps: 
   * - If press button "Ngày hôm qua":
   *  If day == 1
   *  => If month == 1 (January): 
   *    => Set previous_day = 31, previous_month = 12, previous_year = year - 1;
   *    => Output = 31/12/previous_year;
   *  => Else:
   *      => Set previous_month = month - 1
   *      => If previous_month = 3 (March):
   *          => If it's a leap year: set previous_day = 29 => Output = 29/2/year
   *          => Else, set previous_day = 28; => Output = 29/2/year
   *      => Else If previous_month has 30 days: Set previous_day = 30 => Output: 30/previous_month/previous_year
   *      => Else: Set previous_day = 31 => Output: 31/previous_month/previous_year
   *  Else If day != 1
   *      => Set previous_day = day - 1, previous_month = month, previous_year = year;
   *      => Output = previous_day  / previous_month / previous_year;
   *   
   * 
   *  Output: 31/12/2022
   * 
   */
    let day_val = parseInt(document.querySelectorAll(".input_day")[0].value) || 0;
    let month_val = parseInt(document.querySelectorAll(".input_month")[0].value) || 0;
    let year_val = parseInt(document.querySelectorAll(".input_year")[0].value) || 0;
    let result = ``;
    if(day_val === 1) {
      if(month_val===1) {
        result = `31 / 12 / ${year_val - 1}`;
      } else {
        switch(month_val-1) {
          case 1: case 3: case 5: case 7: case 8: case 10:
            result = `31 / ${month_val-1} / ${year}`;
            break; 
          case 2:
            result = `28 / 2 / ${year}`;
            break;
          case 4: case 6: case 9: case 11:
            result = `30 / ${month_val-1} / ${year}`;
            break;
          default:
            result = "Ngày không xác định"
        }
      }
      
    } else {
      result = `${day_val - 1} / ${month_val} / ${year_val}`
    }
    document.querySelectorAll(".txt-result-bai-5")[0].innerHTML = result;
  };

  let bai_5_get_next_day = () => {
    /**
     * 
   * Input:
   * - day = 31
   * - month = 12
   * - year = 2023
   * Steps: 
   * - If press button "Ngày mai":
   *  If month in (1,3,5,7,8,10):
   *    => If day == 31 => Output = 1/month+1/year;
   *    => Else, Ouput = day+1/month/year;
   *  If month in 2:
   *    => If day == 28 => Output = 1/3/year;
   *    => Else: Output = day + 1 / 2 /year;
   *  If month in (4,6,9,11):
   *    => If day == 30 => Output = 1/month+1/year;
   *    => Else: Output = day+1/month/year
   *  If month == 12:
   *    => If day == 31 => Output = 1/1/year+1;
   *    => Else: Output = day+1/month/year;
   *
   * 
   *  Output: 1/1/2024
   * 
   */
    let day_val = parseInt(document.querySelectorAll(".input_day")[0].value) || 0;
    let month_val = parseInt(document.querySelectorAll(".input_month")[0].value) || 0;
    let year_val = parseInt(document.querySelectorAll(".input_year")[0].value) || 0;
    let result = ``;
    switch(month_val) {
      case 1: case 3: case 5: case 7: case 8: case 10:
        result = (day_val === 31 ? `1/${month_val+1}/${year_val}` : `${day_val+1}/${month_val}/${year_val}`);
        break;
      case 2:
        result = (day_val===28 ? (result = `1/3/${year_val}`) : `${day_val+1}/2/${year_val}`);
        break;
      case 4: case 6: case 9: case 11:
        result = (day_val === 30 ? `1/${month_val+1}/${year_val}` : `${day_val+1}/${month_val}/${year_val}`);
        break;
      case 12:
        result = (day_val === 31 ? `1/1/${year_val+1}` : `${day_val+1}/12/${year_val}`);
        break;
      default:
        result = 'Ngày không xác định';
    }
    document.querySelectorAll(".txt-result-bai-5")[0].innerHTML = result;
  };
  let bai_6 = () => {
    /**
     * 
   * Input:
   * - month = 2
   * - year = 2032
   * Steps: 
   * - Declare and Initialize 2 variables to store 2 inputs for month and year.
   * - Declare and Initialize a variable to store output.
   *  Output: Tháng 2 năm 2032 có 29 ngày
   * 
   */
    let month_val = parseInt(document.querySelectorAll(".bai-tap-6 .input_month")[0].value) || 0;
    let year_val = parseInt(document.querySelectorAll(".bai-tap-6 .input_year")[0].value) || 0;
    let result = '';
    switch(month_val) {
      case 1: case 3: case 5: case 7: case 8: case 10: case 12:
        result = `Tháng ${month_val} năm ${year_val} có 31 ngày`;
        break;
      case 2:
        if(!(year_val%400)) {
          result = `Tháng 2 năm ${year_val} có 29 ngày`
        } else if(!(year_val%100)) {
          result = `Tháng 2 năm ${year_val} có 28 ngày`
        } else if(!(year_val%4)) {
          result = `Tháng 2 năm ${year_val} có 29 ngày`
        } else {
          result = `Tháng 2 năm ${year_val} có 28 ngày`
        }
        break;
      case 4: case 6: case 9: case 11:
        result = `Tháng ${month_val} năm ${year_val} có 30 ngày`;
        break;
    }
    document.querySelectorAll(".txt-result-bai-6")[0].innerHTML = result;
  };

  let bai_7 = () => {
    let input_num = (parseInt(document.querySelectorAll(".bai-tap-7 .input-num")[0].value)) || 0;
    console.log("🚀 ~ file: index.js ~ line 292 ~ main ~ input_num", input_num)
    let a = Math.floor(input_num / 100);
    let b = Math.floor((input_num%100)/10);
    let c = Math.floor((input_num%100)%10);
    let result = '';
    switch(a) {
      case 1:
        result+= 'một trăm';
        break;
      case 2:
        result+= 'hai trăm';
        break;
      case 3:
        result+= 'ba trăm';
        break;
      case 4:
        result+= 'bốn trăm';
        break;
      case 5:
        result+= 'năm trăm';
        break;
      case 6:
        result+= 'sáu trăm';
        break;
      case 7:
        result+= 'bảy trăm';
        break;
      case 8:
        result+= 'tám trăm';
        break;
      case 9:
        result+= 'chín trăm';
        break;
      default:
        alert("Hàng trăm không xác định được");
    }
    result += " ";
    switch(b) {
      case 0:
        result+= 'lẻ';
        break;
      case 1:
        result+= 'một mươi';
        break;
      case 2:
        result+= 'hai mươi';
        break;
      case 3:
        result+= 'ba mươi';
        break;
      case 4:
        result+= 'bốn mươi';
        break;
      case 5:
        result+= 'năm mươi';
        break;
      case 6:
        result+= 'sáu mươi';
        break;
      case 7:
        result+= 'bảy mươi';
        break;
      case 8:
        result+= 'tám mươi';
        break;
      case 9:
        result+= 'chín mươi';
        break;
      default:
        alert("Hàng chục không xác định được");
    }
    result += " ";
    switch(c) {
      case 0:
        result+= "";
        break;
      case 1:
        result+= 'một';
        break;
      case 2:
        result+= 'hai';
        break;
      case 3:
        result+= 'ba';
        break;
      case 4:
        result+= 'bốn';
        break;
      case 5:
        result+= 'năm';
        break;
      case 6:
        result+= 'sáu';
        break;
      case 7:
        result+= 'bảy';
        break;
      case 8:
        result+= 'tám';
        break;
      case 9:
        result+= 'chín';
        break;
      default:
        alert("Hàng đơn vị không xác định được");
    }
    document.querySelectorAll(".txt-result-bai-7")[0].innerHTML = result;
  }
  let bai_8 = () => {
    let inputName_1 = document.querySelectorAll('.student1-name')[0].value || '';
    let inputX_1 = parseFloat(document.querySelectorAll('.student1-x-coord')[0].value) || 0;
    let inputY_1 = parseFloat(document.querySelectorAll('.student1-y-coord')[0].value) || 0;

    let inputName_2 = document.querySelectorAll('.student2-name')[0].value || '';
    let inputX_2 = parseFloat(document.querySelectorAll('.student2-x-coord')[0].value) || 0;
    let inputY_2 = parseFloat(document.querySelectorAll('.student2-y-coord')[0].value) || 0;

    let inputName_3 = document.querySelectorAll('.student3-name')[0].value || '';
    let inputX_3 = parseFloat(document.querySelectorAll('.student3-x-coord')[0].value) || 0;
    let inputY_3 = parseFloat(document.querySelectorAll('.student3-y-coord')[0].value) || 0;

    let inputX_4 = parseFloat(document.querySelectorAll('.school-x-coord')[0].value) || 0;
    let inputY_4 = parseFloat(document.querySelectorAll('.school-y-coord')[0].value) || 0;

    let distance_1 = Math.sqrt(Math.pow(inputX_1 - inputX_4, 2) + Math.pow(inputY_1 - inputY_4, 2));
    let distance_2 = Math.sqrt(Math.pow(inputX_2 - inputX_4, 2) + Math.pow(inputY_2 - inputY_4, 2));
    let distance_3 = Math.sqrt(Math.pow(inputX_3 - inputX_4, 2) + Math.pow(inputY_3 - inputY_4, 2));

    let result = '';
    if(distance_1 >= distance_2 && distance_2 >= distance_3) {
      result = `Sinh viên xa trường nhất: ${inputName_1}`;
    } else if(distance_1 >= distance_3 && distance_3 >= distance_2) {
      result = `Sinh viên xa trường nhất: ${inputName_1}`;
    } else if(distance_2 >= distance_1 && distance_1 >= distance_3) {
      result = `Sinh viên xa trường nhất: ${inputName_2}`;
    } else if(distance_2 >= distance_3 && distance_1 >= distance_1) {
      result = `Sinh viên xa trường nhất: ${inputName_2}`;
    } else if(distance_3 >= distance_1 && distance_1 >= distance_2) {
      result = `Sinh viên xa trường nhất: ${inputName_3}`;
    } else {
      result = `Sinh viên xa trường nhất: ${inputName_3}`;
    }
    document.querySelectorAll(".txt-result-bai-8")[0].innerHTML = result;
  }

  document
    .querySelectorAll(".btn-calc-bai-1 ")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_1();
    });

  // giai bai 2
  document
    .querySelectorAll(".btn-calc-bai-2")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_2();
    });

  // giai bai 3
  document
    .querySelectorAll(".btn-calc-bai-3")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_3();
    });

  // giai bai 4
  document
    .querySelectorAll(".btn-calc-bai-4")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_4();
    });
  // giai bai 5
  document
    .querySelectorAll(".btn-calc-bai-5-previous-day")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_5_get_previous_day();
    });
  
  document
    .querySelectorAll(".btn-calc-bai-5-next-day")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_5_get_next_day();
    });
  // giai bai 6
  document
  .querySelectorAll(".btn-calc-bai-6")[0]
  .addEventListener("click", (e) => {
    e.preventDefault();
    bai_6();
  });
  // giai bai 7
  document
  .querySelectorAll(".btn-calc-bai-7")[0]
  .addEventListener("click", (e) => {
    e.preventDefault();
    bai_7();
  });
  // giai bai 8
  document
  .querySelectorAll(".btn-calc-bai-8")[0]
  .addEventListener("click", (e) => {
    e.preventDefault();
    bai_8();
  });
};

main();